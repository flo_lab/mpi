<div id="contentCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
  <div class="carousel-inner" role="listbox">
      <div class="item active">
        <p>home</p>
      </div>
      <div class="item">
        <p>about us</p>
      </div>
      <div class="item">
        <p>products</p>
      </div>
      <div class="item">
        <p>marketing plan</p>
      </div>
      <div class="item">
        <p>contact us</p>
      </div>
      <div class="item">
        <p>login</p>
      </div>
  </div>
  <ol class="carousel-indicators">
    <li data-target="#contentCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#contentCarousel" data-slide-to="1"></li>
    <li data-target="#contentCarousel" data-slide-to="2"></li>
    <li data-target="#contentCarousel" data-slide-to="3"></li>
    <li data-target="#contentCarousel" data-slide-to="4"></li>
    <li data-target="#contentCarousel" data-slide-to="5"></li>
  </ol>
  <a class="left carousel-control" href="#contentCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
  <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#contentCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>